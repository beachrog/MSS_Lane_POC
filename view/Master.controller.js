var recoveryArray = [];
sap.ui.define([
		'jquery.sap.global',
		'sap/m/MessageToast',
		'sap/ui/core/Fragment',
		'sap/ui/core/mvc/Controller'
	]);
sap.ui.core.mvc.Controller.extend("HQ3MSSLane.view.Master", {

	handleFilterPress: function(oEvent) {
		if (!this._oDialog) {
			this._oDialog = sap.ui.xmlfragment("HQ3MSSLane.view.filter", this);
			// Set initial and reset value for Slider in custom control
			var filterValueList = this.getView().byId("list");
		var aItems = filterValueList.getItems();
		var items = [];
		for (var itemCount = 0; itemCount < aItems.length; itemCount++) {
			var empName = filterValueList.getItems()[itemCount].mAggregations.content[0].getItems()[2].mAggregations.items[0].mProperties.text.toString();
			items.push(empName);
		}
		var v = this.getView();
		var m = new sap.ui.model.json.JSONModel();
		m.setData(items);
		v.byId("list").setModel(m);
		//v.byId("DIALOG").open();
		//var oFilter = this._oDialog.getFilterItems()[0].getCustomControl();
		
    	              
      // Create Aggregation Binding
/*			var oList = this._oDialog.getFilterItems()[0].getCustomControl();      
      oList.setModel(oModel );
      oList.bindAggregation("items","/filterValueList");*/

//			oList.setValue(this.filterValueList);
/*			var oSlider = this._oDialog.getFilterItems()[0].getCustomControl();
			oSlider.setValue(this.filterResetValue);*/
		}

		this._oDialog.setModel(this.getView().getModel());
		
		// toggle compact style
		jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialog);
		this._oDialog.open();
	},

	onInit: function() {
		this.oInitialLoadFinishedDeferred = jQuery.Deferred();

		var oEventBus = this.getEventBus();
		oEventBus.subscribe("Detail", "TabChanged", this.onDetailTabChanged, this);

		var oList = this.getView().byId("list");
		oList.attachEvent("updateFinished", function() {
			this.oInitialLoadFinishedDeferred.resolve();
			oEventBus.publish("Master", "InitialLoadFinished");
		}, this);

		//On phone devices, there is nothing to select from the list. There is no need to attach events.
		if (sap.ui.Device.system.phone) {
			return;
		}

		this.getRouter().attachRoutePatternMatched(this.onRouteMatched, this);

		oEventBus.subscribe("Detail", "Changed", this.onDetailChanged, this);
		oEventBus.subscribe("Detail", "NotFound", this.onNotFound, this);
	},

	onRouteMatched: function(oEvent) {
		var sName = oEvent.getParameter("name");

		if (sName !== "main") {
			return;
		}

		//Load the detail view in desktop
		this.loadDetailView();

		//Wait for the list to be loaded once
		this.waitForInitialListLoading(function() {

			//On the empty hash select the first item
			this.selectFirstItem();

		});

	},

	onDetailChanged: function(sChanel, sEvent, oData) {
		var sEntityPath = oData.sEntityPath;
		//Wait for the list to be loaded once
		this.waitForInitialListLoading(function() {
			var oList = this.getView().byId("list");

			var oSelectedItem = oList.getSelectedItem();
			// The correct item is already selected
			if (oSelectedItem && oSelectedItem.getBindingContext("mainModel").getPath() === sEntityPath) {
				return;
			}

			var aItems = oList.getItems();

			for (var i = 0; i < aItems.length; i++) {
				if (aItems[i].getBindingContext("mainModel").getPath() === sEntityPath) {
					oList.setSelectedItem(aItems[i], true);
					break;
				}
			}
		});
	},

	onDetailTabChanged: function(sChanel, sEvent, oData) {
		this.sTab = oData.sTabKey;
	},

	loadDetailView: function() {
		this.getRouter().myNavToWithoutHash({
			currentView: this.getView(),
			targetViewName: "HQ3MSSLane.view.Detail",
			targetViewType: "XML"
		});
	},

	waitForInitialListLoading: function(fnToExecute) {
		jQuery.when(this.oInitialLoadFinishedDeferred).then(jQuery.proxy(fnToExecute, this));
	},

	onNotFound: function() {
		this.getView().byId("list").removeSelections();
	},

	selectFirstItem: function() {
		var oList = this.getView().byId("list");
		var aItems = oList.getItems();
		if (aItems.length) {
			oList.setSelectedItem(aItems[0], true);
			//Load the detail view in desktop
			this.loadDetailView();
			oList.fireSelect({
				"listItem": aItems[0]
			});
		} else {
			this.getRouter().myNavToWithoutHash({
				currentView: this.getView(),
				targetViewName: "HQ3MSSLane.view.NotFound",
				targetViewType: "XML"
			});
		}

		recoveryArray = this.getView().byId("list").getItems();
	},

	onSearch: function() {
					this.oInitialLoadFinishedDeferred = jQuery.Deferred();
		// Add search filter
		/*var filters = [];
		var searchString = this.getView().byId("searchField").getValue();
		if (searchString && searchString.length > 0) {
			filters = [new sap.ui.model.Filter("EMP_Name", sap.ui.model.FilterOperator.Contains, searchString)];
		}
		// Update list binding
		this.getView().byId("list").getBinding("items").filter(filters);

		//On phone devices, there is nothing to select from the list
		if (sap.ui.Device.system.phone) {
			return;
		}

		//Wait for the list to be reloaded
		this.waitForInitialListLoading(function() {
			//On the empty hash select the first item
			this.selectFirstItem();
		});
*/
		//window.recoveryArray = [];

		this.oInitialLoadFinishedDeferred = jQuery.Deferred();
		var searchString = this.getView().byId("searchField").getValue();
		var masterScreenList = this.getView().byId("list");
		var items = [];
		for (var i = 0; i < masterScreenList.getItems().length; i++) {
			masterScreenList.getItems()[i].setVisible(false);
		}
		for (var k = 0; k < recoveryArray.length; k++) {
			masterScreenList.getItems()[k].setVisible(true);
		}
		for (var itemCount = 0; itemCount < masterScreenList.getItems().length; itemCount++) {
			for (var detail = 0; detail < masterScreenList.getItems()[itemCount].mAggregations.content[0].getItems()[2].mAggregations.items.length; detail++) {
				//			for (var detail = 0; detail < 3; detail++) {
				// var empName = masterScreenList.getItems()[i].mAggregations.content[0].getItems()[2].mAggregations.items[detail].mProperties.text;
				// detail++;
				// var empPernr = masterScreenList.getItems()[i].mAggregations.content[0].getItems()[2].mAggregations.items[detail].mProperties.text;
				// detail++;
				// var supName = masterScreenList.getItems()[i].mAggregations.content[0].getItems()[2].mAggregations.items[detail].mProperties.text;
				// detail++;
				// var empType = masterScreenList.getItems()[i].mAggregations.content[0].getItems()[2].mAggregations.items[detail].mProperties.text;
				// detail++;
				// var empEmail = masterScreenList.getItems()[i].mAggregations.content[0].getItems()[2].mAggregations.items[detail].mProperties.text;

				// if (empName.toString().toUpperCase().indexOf(searchString.toUpperCase()) === -1 && empPernr.toString().toUpperCase().indexOf(
				// 		searchString.toUpperCase()) === -1 && supName.toString().toUpperCase().indexOf(searchString.toUpperCase()) === -1 
				// 		&& empType.toString().toUpperCase().indexOf(searchString.toUpperCase()) === -1
				// 		&& empEmail.toString().toUpperCase().indexOf(searchString.toUpperCase()) === -1) {
				if (masterScreenList.getItems()[itemCount].mAggregations.content[0].getItems()[2].mAggregations.items[detail].mProperties.text.toString()
					.toUpperCase()
					.indexOf(searchString.toUpperCase()) === -1) {
					if (detail === (masterScreenList.getItems()[itemCount].mAggregations.content[0].getItems()[2].mAggregations.items.length - 1)) {
						var aItem = masterScreenList.getItems()[itemCount];
						items.push(aItem);
					}

				} else {
					//			detail++;
					break;
				}
			}
		}
		for (var j = 0; j < items.length; j++) {
			items[j].setVisible(false);
		}
		//On phone devices, there is nothing to select from the list  
		if (sap.ui.Device.system.phone) {
			return;
		}

	},

	onSelect: function(oEvent) {
		// Get the list item either from the listItem parameter or from the event's
		// source itself (will depend on the device-dependent mode)
		this.showDetail(oEvent.getParameter("listItem") || oEvent.getSource());
		
	},

	showDetail: function(oItem) {
		// If we're on a phone device, include nav in history
		var bReplace = jQuery.device.is.phone ? false : true;
		this.getRouter().navTo("detail", {
			from: "master",
			entity: oItem.getBindingContext("mainModel").getPath().substr(1),
			tab: this.sTab
		}, bReplace);
	},

	getEventBus: function() {
		return sap.ui.getCore().getEventBus();
	},

	getRouter: function() {
		return sap.ui.core.UIComponent.getRouterFor(this);
	},

	onExit: function(oEvent) {
		var oEventBus = this.getEventBus();
		oEventBus.unsubscribe("Detail", "TabChanged", this.onDetailTabChanged, this);
		oEventBus.unsubscribe("Detail", "Changed", this.onDetailChanged, this);
		oEventBus.unsubscribe("Detail", "NotFound", this.onNotFound, this);
	}
});